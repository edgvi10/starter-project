<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title><?php echo ((isset($page->title)) ? "{$page->title} &ndash; " : null)."{$app->name}"; ?></title>
	<meta name="description" content="<?php echo (isset($page->description))? "{$page->description}" : "{$app->description}"; ?>">

	<!-- Bootstrap and Project CSS -->
	<link rel="stylesheet" href="<?php echo "{$app->plugins}/bootstrap/bootstrap.min.css"; ?>">
	<link rel="stylesheet" href="<?php echo "{$app->assets}/css/style.css"; ?>">

	<!-- Font Awesome CSS including ALL -->
	<link rel="stylesheet" href="<?php echo "{$app->plugins}/fontawesome/css/all.min.css"; ?>">
<?php
	if(isset($page->stylesheets)):
		foreach($page->stylesheets as $link):
?>
	<link rel="stylesheet" href="<?php echo "{$link}"; ?>">
<?php
		endforeach;
	endif;
?>
	<!-- jQuery and Bootstrap -->
	<script src="<?php echo "{$app->plugins}/jquery/jquery.min.js"; ?>"></script>
	<script src="<?php echo "{$app->plugins}/bootstrap/bootstrap.bundle.min.js"; ?>"></script>
<?php
	if(isset($page->js_head)):
		foreach($page->js_head as $link):
?>
	<script src="<?php echo "{$link}"; ?>"></script>
<?php
		endforeach;
	endif;
?>
</head>
<body<?php if(isset($page->body_class)) echo ' class="'.((is_array($page->body_class)) ? implode(" ", $page->body_class) : "{$page->body_class}").'"'; ?>>
