<?php

	setlocale(LC_TIME, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese');
	date_default_timezone_set('America/Sao_Paulo');

	header('Access-Control-Allow-Origin: *');
	header('Charset: utf-8');
	header("Content-Type: text/html; charset=utf-8");

	$app = (object) [];
	$app->name = "Project name";
	$app->description = "Project description with maximum of 140 characters.";
	$app->version = "0.0.1";
	$app->url = "http://project_url";
	$app->api = "http://project_api_url";
	$app->assets = "{$app->url}/assets";
	$app->plugins = "{$app->url}/assets/plugins";

	$page = (object) [];
	$page->header = true;
	$page->menu = true;
	$page->footer = true;
